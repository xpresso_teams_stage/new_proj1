from setuptools import setup

with open('requirements/requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

version_file_name = 'VERSION'
version_fs = open(version_file_name)
version = version_fs.read().strip()
version_fs.close()

# Setup configuration
setup(
    name='dnn_lib',
    version=version,
    packages=['dnn_lib'],
    description="Sample library code",
    install_requires=requirements
)