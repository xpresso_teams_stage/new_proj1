#! /bin/bash
## This script is used to test the docker container once it is up and running



DOCKER_IMAGE_NAME=${1}
TAG=${2}
current_folder=${ROOT_FOLDER}/xprbuild/docker

if [[ -z "$DOCKER_IMAGE_NAME" ]]
then
	DOCKER_IMAGE_NAME=${COMPONENT_NAME}
fi
if [[ -z "$TAG" ]]
then
	TAG=${PROJECT_VERSION}
fi


## Write your test cases here
