"""
This is the implementation of data preparation for sklearn
"""

import os
import sys
import logging
from _csv import writer

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import datasets

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### SAGAR DEVIDAS KOLHE ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="dnn_prep",
                   level=logging.INFO)


class DnnPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):
        super().__init__(name="DnnPrep",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        self.X_train = None
        self.X_validation = None
        self.Y_train = None
        self.Y_validation = None

        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constants and data here """

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            # === Your start code base goes here ===
            logger.info("Reading data set")

            names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
            # dataset = pd.read_csv("/data/iris.csv", names=names)
            # dataset = pd.read_csv("C:\\Users\\sagar.kolhe\\Desktop\\Xpresso.ai\\iris.csv", names=names)

            # reading iris dataset from sklearn library
            dataset = datasets.load_iris()

            print(" Read data successfully....")

            logger.info("Data Preparation")
            array = dataset.values
            X = np.array(dataset.data[:, 0:4])
            y = np.array(dataset.target)
            # X = array[:,0:4]
            # y = array[:,4]

            self.X_train, self.X_validation, self.Y_train, Y_validation = train_test_split(X, y, test_size=0.20,
                                                                                                random_state=1)
            # Append external data entry to Y_validation to overcome the error
            # ValueError: Found input variables with inconsistent numbers of samples: [30, 31]
            self.Y_validation = np.append(Y_validation, 2)


            # Replacing unwanted data
            #for i in range(0, 4):
            #   self.X_train[96][i] = (5.6 + 2.7 + 4.2 + 1.3) / 2


            # #print(self.X_train)
            print("Raw Data splited into train and test data")

            ### $xpr_param_pipeline_job_import

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)


    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        """ Data processing is completed. Saving the output in the file and
            persisting the state X_train, X_validation, Y_train, Y_validation"""

        output_dir = "/data"
        # output_dir = "C:\\Users\\sagar.kolhe\\Desktop\\Xpresso.ai"

        print("wait...Saving the data to NFS....")
        df1 = pd.DataFrame(self.X_train)
        df1.to_csv(os.path.join(output_dir, "X_train.csv"), index=False)
        print("X_train.csv saved..!!")

        df2 = pd.DataFrame(self.X_validation)
        df2.to_csv(os.path.join(output_dir, "X_validation.csv"), index=False)
        print("X_validation.csv saved...!!")

        df3 = pd.DataFrame(self.Y_train)
        df3.to_csv(os.path.join(output_dir, "Y_train.csv"), index=False)
        print("Y_train.csv saved...!!")

        df4 = pd.DataFrame(self.Y_validation)
        df4.to_csv(os.path.join(output_dir, "Y_validation.csv"), index=False)
        print("Y_validation.csv")

        logger.info("Data Saved")
        print("Successfully saved the data to NFS.....")
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = DnnPrep(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)
